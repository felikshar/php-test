<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	require_once(dirname(__FILE__) . '/DB.php');

	$db = DB::getInstance();
?>












<!DOCTYPE html>
<html>
<head>
	<title>TEST-APP</title>
	<link rel="stylesheet" type="text/css" href="./style.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
  
</head>
<body>
	<div class="clearfix">
		<div class="pull-left" id="complexity">
			<section class="range-slider">
				<span class="rangeValues"></span>
				<br>
				<span class="success"></span>
				<input class="complexity-range" value="50" min="50" max="90" step="1" type="range">
				<input class="complexity-range" value="90" min="50" max="90" step="1" type="range">
			</section>
			<div class="text-center">
				<button id="setComplexity">Set Complexity</button>
			</div>
		</div>
		
		<div class="pull-left" id="intelligence">
			<section>
				<span class="rangeValues">User Intelligence: 50</span>
				<br>
				<span class="success"></span>
				<input id="intelligence-range" min="0" max="100" step="1" type="range">
			</section>
			<div class="text-center">
				<button id="setUserIntelligence">Set User Intelligence</button>
			</div>
		</div>
	</div>

	<div id="test-result"></div>

	<table id="test" class="display" style="width:100%; display: none;">
        <thead>
            <tr>
                <th>ID теста</th>
                <th>Тест был показан</th>
                <th>Сложность Теста</th>
                <th>Результат Пользователя</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ID теста</th>
                <th>Тест был показан</th>
                <th>Сложность Теста</th>
                <th>Результат Пользователя</th>
            </tr>
        </tfoot>
    </table>



	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
	<script src="./script.js"></script>
</body>
</html>