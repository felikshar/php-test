<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);


require_once(dirname(__FILE__) . '/DB.php');

$db = DB::getInstance();



if(isset($_POST)) {
	$intelligence = 0;

	if(!empty($_POST['intelligence'])) {
		$intelligence = $_POST['intelligence'];
	}
		
	$tests = $db->getRandomTests($intelligence);
}

echo json_encode($tests);