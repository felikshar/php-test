function getVals(){
  // Get slider values
  var parent = this.parentNode;
    var slides = parent.getElementsByClassName("complexity-range");
    var slide1 = parseFloat( slides[0].value );
    var slide2 = parseFloat( slides[1].value );
  // Neither slider will clip the other, so make sure we determine which is larger
  if( slide1 > slide2 ){ var tmp = slide2; slide2 = slide1; slide1 = tmp; }
  
  var displayElement = parent.getElementsByClassName("rangeValues")[0];
      displayElement.innerHTML = "Complexity Range: " + slide1 + " - " + slide2;
}



$(document).ready(function() {

  var slides = document.getElementsByClassName("complexity-range");
  var slide1 = parseFloat( slides[0].value );
  var slide2 = parseFloat( slides[1].value );

  // Initialize Sliders
  var sliderSections = document.getElementsByClassName("range-slider");
  for( var x = 0; x < sliderSections.length; x++ ){
    var sliders = sliderSections[x].getElementsByTagName("input");
    for( var y = 0; y < sliders.length; y++ ){
      if( sliders[y].type ==="range" ){
        sliders[y].oninput = getVals;
        // Manually trigger event first time to display values
        sliders[y].oninput();
      }
    }
  }

  $("#intelligence-range").change(function(e) {
    var intelligence = e.target.value;
    $("#intelligence .rangeValues").html("User Intelligence: " + intelligence);
  });

  $('#setComplexity').click(function () {
    var slides = document.getElementsByClassName("complexity-range");
    var min = parseFloat( slides[0].value );
    var max = parseFloat( slides[1].value );

    $.ajax({
      url: 'setComplexity.php',
      method: 'post',
      data: {
        min: min,
        max: max
      },
      success: function() {
        $("#complexity .success").html("READY");
        setTimeout(function() {$("#complexity .success").html("");}, 2000)
      }
    });
  })

  var dataTable;
  $('#setUserIntelligence').click(function () {
    var userIntelligence = $('#intelligence-range').val();
    console.log(userIntelligence)

    $.ajax({
      url: 'getTests.php',
      method: 'post',
      data: {
        intelligence: userIntelligence
      },
      success: function(data) {
        data = JSON.parse(data);

        $("#test-result").html("РЕЗУЛЬТАТ: Пройдено тестов " + data.testPassed + "/40. По Процентам: " + parseInt(data.testPassed/40*100) + "%");

        if(dataTable) dataTable.destroy();
        
        dataTable = $('#test').show().DataTable({
          data: data.result,
          columns: [
              { data: 'id' },
              { data: 'selected' },
              { data: 'complexity' },
              { data: 'test_passed' }
          ]
        });
      }
    });
  })
  
});
