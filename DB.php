<?php

require_once(dirname(__FILE__) . '/configs.php');

class DB
{
    private $connection;
    private static $_instance;
    private $dbHost = __HOST__;
    private $dbUser = __USER__;
    private $dbPass = __PASSWORD__;
    private $dbName = __DATABASE__;

    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance() {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    // Constructor
    private function __construct() {
        try {
            $this->connection = new PDO('mysql:host=' . $this->dbHost, $this->dbUser, $this->dbPass);
            $this->createAndUseDataBase($this->connection, $this->dbName);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->createTable($this->connection, 'questions', ['complexity' => 'INT', 'selected' => 'INT']);
            if($this->getTestsCount($this->connection) != 100) {
                $this->generateTests($this->connection);
            }
            

        } catch (PDOException $e) {
            die("Failed to connect to DB: " . $e->getMessage());
        }
    }

    private function __clone() {
        return false;
    }

    private function __wakeup() {
        return false;
    }

    public function createAndUseDataBase($connection, $dbname) {
        $sql = 'CREATE DATABASE IF NOT EXISTS ' . $dbname;

        if ($connection->query($sql) !== false) {
            $connection->query('USE ' . $dbname);
            return 1;
        } else {
            return 0;
        }
    }

    public function createTable($connection, $table, $fields) {
        $sql = "CREATE TABLE IF NOT EXISTS `$table` (";
        $sql .= '`id` INT AUTO_INCREMENT PRIMARY KEY,';

        foreach ($fields as $field => $type) {
            $sql .= "`$field` $type,";
        }

        $sql = rtrim($sql, ',') . ') CHARACTER SET utf8 COLLATE utf8_general_ci';

        if ($connection->exec($sql) !== false) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getTestsCount($connection, $testsCount = 100) {
        $sql = "SELECT COUNT(*) as count FROM `questions`";
        $result = $connection->query($sql)->fetch(PDO::FETCH_OBJ);

        return $result->count;
    }

    public function generateTests($connection, $testsCount = 100) {
        $sql = "INSERT INTO questions (`complexity`, `selected`) VALUES (0, 0)";

        for ($i = 1; $i < $testsCount; $i++) {
            $sql .= ", (0, 0)";
        }

        $sql .= ";";

        if ($connection->query($sql) !== false) {
            return 1;
        } else {
            return 0;
        }
    }

    public function setComplexityTests($min, $max, $testsCount = 100)
    {

        $sql = "UPDATE `questions` SET `complexity` = CASE id";

        for ($i = 1; $i <= $testsCount; $i++) {
            $random = mt_rand($min, $max);
            $sql .= "
                WHEN '$i' THEN '$random'
            ";
        }

        $sql .= "ELSE `complexity` END";

        if ($this->connection->query($sql) !== false) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getRandomTests($intelligence, $testsCount = 40)
    {
        $selectedCountSQL = "SELECT SUM(`selected`) AS total FROM `questions`";
        $sql = "SELECT * FROM `questions`";

        $selectedCountOld = $this->connection->query($selectedCountSQL)->fetch(PDO::FETCH_OBJ)->total;
        if ($selectedCountOld == 0) $selectedCountOld = 100;
        $result = $this->connection->query($sql)->fetchAll(PDO::FETCH_OBJ);

        $selectedCountNew = 0;
        $filteredResult = [];

        foreach ($result as $row) {
            $row->reverseSelected = $selectedCountOld - $row->selected;
            $selectedCountNew += $row->reverseSelected;
        }

        $testPassed = 0;
        while (count($filteredResult) < 40) {
            foreach ($result as $key => $row) {
                $probability = rand(1, 10000)/100;
                if ($probability <= $row->reverseSelected / $selectedCountNew * 100) {
                    if ($row->complexity < $intelligence) {
                        $testPassed++;
                        $row->test_passed = "Пройден";
                    } else {
                        $row->test_passed = "Провален";
                    }
                    unset($row->reverseSelected);
                    $filteredResult[] = $row;
                    unset($result[$key]);
                    break;
                }
            }
        }

        $updateSQL = "UPDATE `questions` SET `selected` = CASE id";
        foreach ($filteredResult as $row) {
            $id = $row->id;
            $selected = $row->selected + 1;
            $updateSQL .= "
                WHEN '$id' THEN '$selected'
            ";
        }
        $updateSQL .= "ELSE `selected` END";

        $this->connection->query($updateSQL);


        usort($filteredResult, function($a, $b) { return $a->id - $b->id; });
        return ['result' => $filteredResult, 'testPassed' => $testPassed];
    }

    public function __destruct() {
        $this->connection = null;
    }
}