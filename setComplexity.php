<?php

require_once(dirname(__FILE__) . '/DB.php');

$db = DB::getInstance();

if(isset($_POST)) {
	$min = 0;
	$max = 0;
	
	if(!empty($_POST['min'])) {
		$min = $_POST['min'];
	}

	if(!empty($_POST['max'])) {
		$max = $_POST['max'];
	}
	
	$a = $db->setComplexityTests($min, $max);
}